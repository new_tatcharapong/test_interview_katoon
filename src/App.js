import React from 'react';
import logo from './logo.svg';
import './App.css';
import ShowTable from './Components/showTable';

function App() {
  return (
    <div className="App">
        <ShowTable></ShowTable>
    </div>
  );
}

export default App;
